# reg-app - Solution for Assessment 1

# Question

REQUIREMENTS:

1. Mandatory form fields (with HTML5 input types)
  a. Email (email)
  b. Password (password)
  c. Name (text)
  d. Gender (radio)
  e. Date of Birth (date)
  f. Address (text)
  g. Country (text or select)
  h. Contact Number (text)
  i. Submit (submit or button)

2. The following field validations are expected (you may decide how to feedback validation error messages to the user)
  a. Mandatory fields are not to be left blank
  b. Email entered must be a valid email address
  c. Password must adhere to the following strong password policies:
    i. have at least 8 characters
    ii. use both uppercase and lowercase letters (case sensitivity)
    iii. include one or more numerical digits
    iv. include one of these special characters @, #, $
  d. Registrant should be at least 18 years old
  e. Contact number should not contain invalid characters (+, -, space, and round brackets are allowed, as they are used for area code and extensions)

3. Information submitted by this registration form should be:
  a. Sent to the server (via GET or POST)
  b. Inform registrant of successful form submission by displaying either a Thank You page, or a Modal, or inline HTML message
  c. Submitted data should be visible on the Express console

# Points to note

1. Do not forget npm init / bower init
When you work in a team, everyone should be aware of the packages that you are using and package.json / bower.json tell them what all packages does this project depend on.
2. Don't forget to add a .gitignore
Adding unnecessary files to your repo will increase the size of the repo such as node_modules, bower_components, .idea folder etc.

    For eg., the `.idea` folder contains meta data related to your project. like..
    * data structures to access files quickly & for auto completion
    * local history
    * ... lots of stuff that other's probably don't need!

    These will be different for all the people working on the project. And keeping this in repo means you'll have to maintain and merge the changes to files.

    Use any free service to generate a comprehensive gitignore file for you such as https://www.gitignore.io/

3. Avoid using variables without declaring them with `var` - it pollutes the global scope (meaning you'll run out of variable names) and is generally considered bad for memory consumption. Believe us, this _WILL_ come to bite you  in production applications!

4. Check empty form submissions. Generally a good practice and shows that you're thorough with your data validation before submitting to an API.

5. Check if data is received on backend as expected and log any error messages for easier debugging. It does not make sense if you only return a 500 error and don't know why it happened.

6. `npm install` VS `npm install --save`

7. Always display error messages when encountered in catch callbacks on the front end.
8. Never expose bower.json / package.json. The world doesn't need to know what packages you're using and neither should you tell them. Move `bower.json` out of the `public` directory and add a static path to the `bower_components` folder in express. 

   `app.use('/bower_components', express.static(__dirname + '/bower_components'));`